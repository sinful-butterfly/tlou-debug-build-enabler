﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PS3Lib;
namespace tlou_debug
{
    public partial class Form1 : Form
    {
        public static CCAPI console = new CCAPI();
        private uint[] procs;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (console.ConnectTarget())
            {
                console.AttachProcess();
                console.Notify(CCAPI.NotifyIcon.FINGER, "Connected and attached!");
                processName.Text = "OK";
                consoleType.Text = console.GetFirmwareType();

            }

            else
            {
                MessageBox.Show("Something went wrong", "Something went wrong", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            byte[] vals = { 0x38, 0x00, 0x00, 0x01, 0x98, 0x0B, 0x2D, 0xB0 };
            console.SetMemory(0x00A92640, vals);
            console.Notify(CCAPI.NotifyIcon.CAUTION, "Debug Build Enabled");
            console.Notify(CCAPI.NotifyIcon.FINGER, "Key combos: L3 + SELECT/L3 + START");
        }
    }
}
